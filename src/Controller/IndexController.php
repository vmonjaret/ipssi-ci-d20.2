<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

class IndexController
{
    public function __invoke()
    {
        return new JsonResponse('OK');
    }
}
